angular.module('admin',[]).config(function($stateProvider){
	$stateProvider.state('admin',{
		url: '/admin',
		templateUrl: 'js/admin/admin.tpl',
		controller: 'AdminCtrl'
	});
}).controller('AdminCtrl', function( $scope, $state, AdminService ) {
	console.log("AdminCtrl");
	$scope.adminCtrl = {
		allPendingPost:[],
		allPendingComment:[]
		
	};
	getPostTOApprove();
	getCommentTOApprove();

	function getPostTOApprove(){
		AdminService.getPostTOApprove().success( function( response ) {
			console.log('success response of getPostTOApprove call: ',response);
			$scope.adminCtrl.allPendingPost=response;
			//console.log('$scope.postCtrl.allPost: ',$scope.postCtrl.allPost);


		} ).error( function( response ) {
			//console.log('error response of getPostTOApprove call: ',response);
		} );
	};


	$scope.approvePost = function(postId){
		console.log('post id before approval',postId);
		AdminService.approvePost(postId).success( function(response) {
			getPostTOApprove();
			console.log('response after post approval: ', response);

		}).error( function(response){
			console.log("error after a post approval",response);
		})
	};

	function getCommentTOApprove(){
		AdminService.getCommentTOApprove().success( function( response ) {
			console.log('success response of getCommentTOApprove call: ',response);
			$scope.adminCtrl.allPendingComment=response;
			console.log('$scope.adminCtrl.allPendingComment ',$scope.adminCtrl.allPendingComment);


		} ).error( function( response ) {
			console.log('error response of getCommentTOApprove call: ',response);
		} );
	};

	$scope.approveComment = function(commentId){
		console.log('comment id before approval',commentId);
		AdminService.approveComment(commentId).success( function(response) {
			getCommentTOApprove();
			console.log('response after comment approval: ', response);

		}).error( function(response){
			console.log("error after a comment approval",response);
		})
	};

});