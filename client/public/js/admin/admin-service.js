angular.module( 'app' ).factory( 'AdminService', function( $http ) {
	return {
		getPostTOApprove: function() {
			return $http.get('http://localhost:3333/post/toverify');
		},
		approvePost: function(postId) {
			return $http.post('http://localhost:3333/post/approve',{postId: postId,username:'admin',password:'admin123'});
		},
		getCommentTOApprove: function(){
			return $http.get('http://localhost:3333/comment/toverify');
		},
		approveComment: function(commentId){
			return $http.post('http://localhost:3333/comment/approve',{commentId: commentId,username:'admin',password:'admin123'});
		}
	};
} );

