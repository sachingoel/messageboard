<div style= "padding-top: 1%;padding-left: 1%;
padding-right: 1%;
padding-bottom: 1%;">
<div><h3><center><label class="label label-default">Admin Panel</label></center></h3></div>
</br>

<h3><center>POSTS</center></h3>
<table class="table table-striped table-hover table-inverse">
		<!--  -->
		<tr  ng-repeat="post in adminCtrl.allPendingPost">
			<td style="width: 85%">{{post.text}}</td>
			<td><button style="float: right;" class="btn btn-xs btn-success glyphicon glyphicon-check" ng-click="approvePost(post._id)">Approve</button></td>
		</tr>
</table>

</br>
<h3><center>COMMENTS</center></h3>
<table class="table table-striped table-hover">
		<!--  -->
		<tr ng-repeat="comment in adminCtrl.allPendingComment">
			<td style="width: 80%">{{comment.text}}</td>
			<td><button style="float: right;" class="btn btn-xs btn-success glyphicon glyphicon-check" ng-click="approveComment(comment._id)">Approve</button></td>
		</tr>
</table>
</div>