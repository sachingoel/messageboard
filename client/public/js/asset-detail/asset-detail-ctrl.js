angular.module('asset-detail',[]).config(function($stateProvider){
		$stateProvider.state('asset-detail',{
			url:'/assetdetail/:id/:datasetCode',
			templateUrl: '/js/asset-detail/asset-detail.tpl',
			controller: 'AssetDetailCtrl'
		});
}).controller('AssetDetailCtrl',function($scope,$state,$stateParams,AssetDetailService){
	console.log('AssetDetailCtrl')

	//console.log('$stateparams.stockname',$stateParams.id)
	//console.log('$stateparams.dataset_code',$stateParams.dataset_code)
		
		$scope.assetDetailCtrl={
			stockName:$stateParams.id,
			datasetCode :$stateParams.datasetCode,
			assetDetails :  {}
		}
		// console.log('stockname in state params',$scope.assetDetailCtrl.stockName)
		// console.log('datasetCode in state params',$scope.assetDetailCtrl.datasetCode)

		var datasetCode =$scope.assetDetailCtrl.datasetCode;
		// console.log('dataset_code  outside method',datasetCode)
		$scope.stockDetails = function(datasetCode,fromDate,toDate){
			
			AssetDetailService.stockDetails(datasetCode,fromDate,toDate).success(function(response){
				//console.log('reponse of asset details',response)
				$scope.assetDetailCtrl.assetDetails = response
			}).error(function(response){
				console.log('error in getting the details of stock',error)
			})
		};
})