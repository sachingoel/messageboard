angular.module('asset',[]).config(($stateProvider)=>{
		$stateProvider.state('asset',{
			url:'/assets',
			templateUrl: '/js/asset/asset.tpl',
			controller: 'AssetCtrl'
		});
}).controller('AssetCtrl',function($scope,$state,AssetService){
		console.log('AssetCtrl');
		$scope.assetCtrl = {
		stocksOnSearch :[],
		commodityOnSearch:[]
	}

		$scope.searchStock = function(stock,stockDb){
			console.log('search value',stock);
			console.log('stockDb value',stockDb);
			console.log('inside search stock controller');
			AssetService.searchStock(stock,stockDb).success(function(response){
				$scope.assetCtrl.stocksOnSearch = response;
				console.log('response of stock search',response);
			}).error(function(response){
				console.log('error in searching stocks',error)
			})
		};

		$scope.searchCommodity = function(commodity){
			console.log('commodity search',commodity);
			AssetService.searchCommodity(commodity).success(function(response){
				$scope.assetCtrl.commodityOnSearch = response;
				console.log('response of commodity search',response);
			}).error(function(response){
				console.log('error in searching commodities',error)
			})
		};

		$scope.toggle = function(arg){
			$scope.toggleShow=arg;
		};
		
})

