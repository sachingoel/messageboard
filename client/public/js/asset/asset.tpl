<div style= "padding-top: 1%;padding-left: 1%;
padding-right: 1%;
padding-bottom: 1%;" >

THIS IS asset LANDING PAGE
</br></br>
<h3> Search Assets </h3>
<button class="btn btn-success" ng-click="toggle('stocks')">Stocks</button>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
<button class="btn btn-success" ng-click="toggle('commodities')">Commodities</button>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
<button class="btn btn-success" ng-click="toggle('currencies')">Currencies</button>

<div ng-show="toggleShow=='stocks'"> <!-- Div start stock search -->

	<form name="stockForm"></br>
		<label> Enter the Stock</label>
		<input type="search" name="stockname" ng-model="stock" placeholder="Search your stock here" required>&emsp; &emsp;

		<label>Select the Database</label>&nbsp;
		<input type="radio" name="database" value="NSE" ng-model="stockDb">NSE</input>&nbsp;&nbsp;
		<input type="radio" name="database" value="BSE" ng-model="stockDb">BSE</input>&nbsp;&nbsp;
		<input type="radio" name="database" value="ALL" ng-model="stockDb">ALL</input>

		<div ng-messages="stockForm.stockname.$error">
			<div ng-message="required">
				Stock is required to perform search
			</div>
		</div>

		<button class="btn btn-primary" ng-click="searchStock(stock,stockDb)" ng-disabled="!(stock&&stockDb)">Search</button>
	</form>
</br>
<div ng-if="assetCtrl.stocksOnSearch.meta.total_count===0" class="error">
	<h5>There are no such stocks in the database</h5>
</div>

<div ng-if="assetCtrl.stocksOnSearch.meta.total_count>0">
</br>
<table class="table table-striped table-hover table-inverse">
	<th>Stock Name</th>
	<th>Database</th>
	<tr  ng-repeat="stock in assetCtrl.stocksOnSearch.datasets">
		<td ui-sref = "asset-detail({id: stock.name,datasetCode:stock.dataset_code})" style="width: 50%">{{stock.name}}</td>
		<td>{{stock.database_code}}
		</tr>
	</table>
</div>
</div> <!-- div end stock search -->


<div ng-show="toggleShow=='commodities'"><!-- div start commodity search -->
	Commodity search </br></br>
		<label>Enter Commodity</label>
		<input type="search" name="commodity" placeholder="Search Commodity" ng-model="commodity"/></br>
		<button class="btn btn-primary" ng-click="searchCommodity(commodity)">Submit</button>
</br>
	<div ng-if="assetCtrl.commodityOnSearch.meta.total_count===0" class="error">
		<h5>There are no such commodities in the database</h5>
	</div>

	<div ng-if="assetCtrl.commodityOnSearch.meta.total_count>0">
	</br>
		<table class="table table-striped table-hover table-inverse">
			<th>commodity Name</th>
			<tr  ng-repeat="commodity in assetCtrl.commodityOnSearch.datasets">
				<!-- <td ui-sref = "asset-detail({id: commodity.name,datasetCode:commodity.dataset_code})" style="width: 50%">{{commodity.name}}</td> -->
				<td style="width:50%">{{commodity.name}}</td>ss
			</tr>
		</table>
	</div>
</div><!-- div end commodity search -->


<div ng-show="toggleShow=='currencies'"><!-- div start currency search -->
	</br>
	<label>Search Currencies</label>
	<input type="search" name="currency" placeholder="Enter Currency"/></br>
	<button class="btn btn-primary" ng-click="searchCurrency()">Submit</button>
</div><!-- div end currency search -->


</div>