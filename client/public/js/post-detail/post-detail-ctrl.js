angular.module('post-detail',[]).config(function($stateProvider){
	$stateProvider.state('post-detail',{
		url: '/post/:id',
		templateUrl: 'js/post-detail/post-detail.tpl',
		controller: 'PostDetailCtrl'
	});
}).controller('PostDetailCtrl', function( $scope, $state , $stateParams, PostDetailService ) {
	console.log("PostDetailCtrl");

	$scope.postDetailCtrl = {
		postId:$stateParams.id,
		comments:[]
	};
	getPost();
	getAllComments();

	console.log('$scope.postDetailCtrl.postId:',$scope.postDetailCtrl.postId);
	
	function getAllComments() {
		PostDetailService.getAllComments($scope.postDetailCtrl.postId).success( function( response ) {
			console.log('success response of getAllCommentsgetAllComments call: ',response);
				$scope.postDetailCtrl.comments=response;
		} ).error( function( response ) {
			console.log('error response of getAllComments call: ',response);
		} );
	};


	function getPost() {
		PostDetailService.getPost($scope.postDetailCtrl.postId).success( function( response ) {
			console.log('success response of getPost call: ',response);
				$scope.post=response;
		} ).error( function( response ) {
			console.log('error response of getPost call: ',response);
		} );
	};

	$scope.commentOnPost = function(postId,commentText){
		console.log('post id before comment',postId);
		PostDetailService.commentOnPost(postId,commentText).success( function(response) {
			//$scope.getAllPost()
			console.log('response after comment on post: ', response);
			alert("Comment saved successfuly, once veryfied by Admin it will get displayed ");
			$state.reload();

		}).error( function(response){
			console.log("error in comment on post",response);
		})
	};

	$scope.replyOnComment = function(postId,replyText, commentId){
		console.log('post id before reply',postId,commentId);
		
		PostDetailService.replyOnComment(postId,replyText).success( function(response) {
			alert("Reply saved successfuly, once veryfied by Admin it will get displayed ");
			$state.reload();
			$scope.postDetailCtrl.commentId = '';


		}).error( function(response){
			console.log("error in reply on comment",response);
		});
	};

	$scope.openCommentBox = function(commentId){
		$scope.postDetailCtrl.commentId = commentId;

	};
	$scope.cancelCommentBox = function(commentId){
		$scope.postDetailCtrl.commentId = '';

	};

});