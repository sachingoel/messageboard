angular.module( 'app' ).factory( 'PostDetailService', function( $http ) {
	return {
		getPost: function(id) {
			console.log("postId: ",id)
			return $http.get('http://localhost:3333/post/'+id);
		},
		commentOnPost: function(postId, commentText){
			console.log("postId, commentText in service: ",postId,commentText);
			return $http.post('http://localhost:3333/post/comment',{text:commentText,postId:postId,authorId:'guest'})
		},
		getAllComments: function(id){
			return $http.get('http://localhost:3333/allcomment/'+id);
		},
		replyOnComment: function(postId, replyText){
			console.log("postId, replyText in service: ",postId,replyText);
			return $http.post('http://localhost:3333/post/comment',{text:replyText,postId:postId,authorId:'guest'})
		}
	};
});
