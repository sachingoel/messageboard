<div style= "padding-top: 5%;padding-left: 2%;
padding-right: 2%;
padding-bottom: 2%;">


<text class="text text-info" style="width:300px" getPost()><h2>{{post.text}}</h2></text><h6><i><b>-By: {{post.authorId}}</i></b><h6>
</br>
</br>
<!-- <textarea  class="form-control" rows="2"  ng-model="postComment" placeholder="Post your comment here" ></textarea></br>
  <button class="btn btn-primary" ng-disabled="!postComment" ng-click="commentOnPost(post._id,postComment)")>POST</button> -->
  <div style="padding-top:2%; margin-left: 38%;" class="form-group">
    <table>
      <th style="width: 80%;">
       <div >
        <textarea class="form-control" style="resize:none;" rows="2" id="comment" ng-model="postComment" placeholder="Post your Comment..." ></textarea>
      </div>
    </th>
    <th>
     <div><button class="btn btn-primary"  style="height: 53px; margin-top: 18px;
      margin-left: 11px;" ng-click="commentOnPost(post._id,postComment)" ng-disabled="!postComment">POST</button></div>
    </th>
  </table>
</div>

<div class="container">
 <div class="blockTitle">
   <h2 ng-if="postDetailCtrl.comments.length > 0" >Comments:</h2>
   <h2 ng-if="postDetailCtrl.comments.length === 0" >No Comments to display</h2>
 </div>


 <div style="padding-left:300px">
   <div class="blockListingLeft" data-ng-repeat="comment in postDetailCtrl.comments">
     <div class="frow">
      <div class="MainactivityBlock wow fadeInUp">
        <div class="actContent">
         <div class="actTitle">
          {{comment.text}}</br>
          <h6><i>&nbsp;&nbsp;&nbsp;&nbsp;-In Reply To: {{post.authorId}}</i><h6>
          </div>
          <div >
            <i class="fa fa-commenting-o" data-ng-click="openCommentBox(comment._id)">&nbsp;Reply</i>
            <!-- <div class="sbHed">Reply</div> -->
          </div>

          <div style="padding-top:5px;" ng-show="postDetailCtrl.commentId === comment._id">
            <!-- textarea for commentbox -->
            <textarea class="form-control" style="resize:none;" id="comment" ng-model="replyComment" placeholder="Your Reply.." ></textarea>

            <button class="btn btn-success"  style=" margin-top: 5px;
            margin-left: 5px;" ng-click="replyOnComment(post._id,replyComment, comment._id)" ng-disabled="!replyComment" >Submit</button>

            <button class="btn btn-success" style=" margin-top: 5px;
            margin-left: 5px;"   ng-click="cancelCommentBox(post._id,replyComment, comment._id)" >Cancel</button>
          </div>

          <div class="actBottom">
            <div>
             <div style=" margin-left: 38%;" class="form-group">
              <table>
               <th style="width: 80%;">
                <!-- <div><button class="btn btn-success"  style="height: 53px; margin-top: 18px;
                margin-left: 11px;" ng-click="openCommentBox(comment._id)" >Reply</button></div> -->
              </th>
              <th>


              </th>
            </table>
          </div>

        </div>
      </div>

    </div>

  </div>
</div>

</div>
</div>