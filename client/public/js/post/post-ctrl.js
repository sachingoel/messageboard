angular.module('post',[]).config(function($stateProvider){
	$stateProvider.state('post',{
		url: '/post',
		templateUrl: 'js/post/post.html',
		controller: 'PostCtrl'
	});
}).controller('PostCtrl', function( $scope, $state,$interval,PostService ) {
	console.log('post controller');
	$scope.postCtrl = {
		post: {},
		allPost:[],
		commentDto:{},
		toggleComment:false,
		feed :[]
	};
	getAllPost();

	$scope.createPost = function() {
		console.log("post to be created: ",$scope.postCtrl.post);
		PostService.createPost($scope.postCtrl.post).success(function(response){
			alert("Post saved successfuly, once veryfied by Admin it will get displayed ");
			$scope.postCtrl.post.text = "";

		}).error(function(response){
			console.log("Error in creating post: ",response);

		});
	};
	function getAllPost() {
		PostService.getAllPost().success( function( response ) {
			console.log('success response of getAllpost call: ',response);
			$scope.postCtrl.allPost=response;
			console.log('$scope.postCtrl.allPost: ',$scope.postCtrl.allPost);


		} ).error( function( response ) {
			console.log('error response of getAllpost call: ',response);
		} );
	};


	$scope.likePost = function(postId){
		console.log('post id before like',postId);
		PostService.likePost(postId).success( function(response) {
			getAllPost()
			console.log('response after liking post: ', response);

		}).error( function(response){
			console.log("error in liking a post",response);
		})
	};

	$scope.offencePost = function(postId){
		console.log('post id before offense',postId);
		PostService.offencePost(postId).success( function(response) {
			getAllPost()
			console.log('response after offense post: ', response);

		}).error( function(response){
			console.log("error in offence a post",response);
		})
	};

	$scope.commentOnPost = function(postId,commentText){
		console.log('post id before comment',postId);
		PostService.commentOnPost(postId,commentText).success( function(response) {
			//$scope.getAllPost()
			console.log('response after comment on post: ', response);

		}).error( function(response){
			console.log("error in comment on post",response);
		})
	};

	$scope.showCommentBox=function(){
		console.log("toggleCommentBox",$scope.postCtrl.toggleComment);
		$scope.postCtrl.toggleComment = true;
	};
	$scope.hideCommetBox=function(){
		console.log("toggleCommentBox",$scope.postCtrl.toggleComment);
		$scope.postCtrl.toggleComment = false;
	};

	$scope.ratePost = function(postId){
		console.log("postId and rating",postId,isRating );
		PostService.ratePost(postId,isRating).success(function(response){
			console.log('response after rating a post: ', response);
		}).error( function(response){
			console.log('error after rating a post: ', response);
		})
	};

	$scope.applyFilter = function(selectedFilter){
		console.log("selectedFilter",selectedFilter);
		if(selectedFilter === "Popular"){
			$scope.sortPostOnPopularity();
		}
		else{
			$scope.sortPostOnDate();
		}
	}

	$scope.sortPostOnPopularity = function(){
		console.log("sortPostOnPopularity");
		PostService.sortPostOnPopularity().success(function(response){
			console.log('response sortPostOnPopularity: ', response);
			$scope.postCtrl.allPost = response;
		}).error(function(response){
		console.log('response sortPostOnPopularity: ', response);
		})
	}

	$scope.sortPostOnDate = function(){
		console.log("sor't/'PostOnDate");
		PostService.sortPostOnDate().success(function(response){
			console.log('response sortPostOnDate: ', response);
			$scope.postCtrl.allPost = response;
		}).error(function(response){
		console.log('response sortPostOnDate: ', response);
		})
	};

	$scope.liveNiftyFromServer = function(){
		console.log("getting live feed of NIFTY from server");
		PostService.liveNiftyFromServer().success(function(response){
			console.log('response of live NIFTY from server: ', response);
			$scope.response = response;
			$scope.postCtrl.feed=response;
		}).error(function(response){
			console.log('error in fetching NIFTY data from server ',response);
		})
	};

	$scope.liveNiftyAndSensex = function(){
		//console.log('insisde nifty controller')
		//console.log("getting live feed of nifty and sensex");
		PostService.liveNiftyAndSensex().success(function(response){
			response = response.replace('//','');
			var response = JSON.parse(response);
			$scope.response = response;
			
		}).error(function(response){
			console.log('error in getting the live feed of NIFY and SENSEX',response);
		})
	};


	// Uncomment the below method to get the real time live feed of nifty and sensex

	// $interval(
	// 	$scope.liveNiftyAndSensex = function(){
	// 	//console.log('insisde nifty controller')
	// 	//console.log("getting live feed of nifty and sensex");
	// 	PostService.liveNiftyAndSensex().success(function(response){
	// 		response = response.replace('//','');
	// 		var response = JSON.parse(response);
	// 		$scope.response = response;
			
	// 	}).error(function(response){
	// 		console.log('error in getting the live feed of NIFY and SENSEX',response);
	// 	})
	// },500 )



} );
