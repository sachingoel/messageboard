angular.module( 'app' ).factory( 'PostService', function( $http , $interval) {
	return {
		getAllPost: function() {
			return $http.get('http://localhost:3333/post/recent');
		},
		createPost: function(postDto) {
			console.log("postDto in server:",postDto)
			return $http.post('http://localhost:3333/post',{text:postDto.text,authorId:'GUEST'});
		},

		likePost : function(postId){
			console.log('post id in service',postId);
			return $http.post('http://localhost:3333/post/like',{postId:postId,userId:'5847d674da6d13318fed7f69'})
		},
		offencePost: function(postId){
			return $http.post('http://localhost:3333/post/offensive',{postId:postId,userId:'5847d674da6d13318fed7f69'})
		},
		commentOnPost: function(postId, commentText){
			console.log("postId, commentText in service: ",postId,commentText);
			return $http.post('http://localhost:3333/post/comment',{text:commentText,postId:postId,authorId:'guest'})
		},
		ratePost: function(postId,postRating){
			console.log("postId, postRating in service",postId,postRating);
			return $http.post('http://localhost:3333/post/rate',{postId:postId,rating:postRating})
		},
		sortPostOnPopularity: function(){
			console.log("sortPostOnPopularity in service");
			return $http.get('http://localhost:3333/post/popular');
		},
		sortPostOnDate: function(){
			console.log("sortPostOnDate in service");
			return $http.get('http://localhost:3333/post/recent');
		},
		liveNiftyFromClient: function(){
			console.log("serive for live nifty feed direct from client side");
			return $http.get('http://finance.google.com/finance/info?client=ig&q=NIFTY');
		},
		liveNiftyFromServer: function(){
			console.log("serive for live nifty feed direct from server side");
			return $http.get('http://localhost:3333/live/NIFTY');
		},
		liveNiftyAndSensex : function(){
			//console.log("service for getting live feed of both Nifty and sensex from client side");
			return $http.get('http://finance.google.com/finance/info?client=ig&q=nifty,sensex');
		}
	};
});
