const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
var SchemaTypes = mongoose.Schema.Types;
const Schema = mongoose.Schema;


const StockModel = new Schema({
	
		"symbol": {type : String},
		"name":{ type : String},
		"data":[{
			"open":{ type : SchemaTypes.Double},
			"high":{type : SchemaTypes.Double},
			"low":{type : SchemaTypes.Double},
			"last":{type : SchemaTypes.Double},
			"close":{type : SchemaTypes.Double},
			"totalTradeQuantity":{type: Number},
			"date" :{type :Date}
		}],
		// "previousClosingPrice" : { type : Number},
		// "openPrice" : { type: Number},
		"bid":{
			"price": {type: Number},
			"quantity" :{ type : Number}
		},
		"offer":{
			"price": {type: Number},
			"quantity" :{ type : Number}
		}
	//,
	// "currency": {
	// 	"name" : {type : String},
	// 	"bid":{
	// 		"price": {type: Number},
	// 		"quantity" :{ type : Number}
	// 	}
	// }
});

const CommoditiesModel = new Schema({
	"name" : {type: String , trim : true},
		"data":[{
			"open":{type : SchemaTypes.Double },
			"high":{type : SchemaTypes.Double },
			"low":{type : SchemaTypes.Double },
			"volume":{type: Number , trim :true },
			"last":{type : SchemaTypes.Double},
			"date":{type:Date}
		}],
		"avgPrice":{type: Number , trim :true },
		"bid":{
			"price": {type: Number},
			"quantity" :{ type : Number}
		}
	});

module.exports = mongoose.model('Stock',StockModel);