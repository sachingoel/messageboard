const Assets = require('./Assets');
const Boom = require('boom');
const Joi = require('joi');
const Quandl = require('quandl');
var Request = require('request');

var quandl = new Quandl({
	auth_token: "afgxAVqsHJf5pfSq4XBz",
	api_version: 3,
	//proxy: "http://localhost:3333"
});

exports.register = function(server,options,next){
	server.route([
	{// Test Route
		method:'GET',
		path:'/fb',
		handler:(request,reply)=>{
			quandl.dataset({
				source: "WIKI",
				table: "FB"
			}, {
				order: "asc",
				exclude_column_names: true,
  						// Notice the YYYY-MM-DD format 
  						start_date: "2016-11-30",
  						end_date: "2016-12-15",
  						column_index: 4
  					}, (err,response)=>{
  						if(err){
  							throw err;
  						}
  						console.log(response);
  						reply(response);
  					});


		}
	},
	{
		method:'GET',
		//path:'/search/stock'
		path:'/search/{stockDb}/{stock}',
		handler:(request,reply)=>{
			var stockDb = request.params.stockDb;
			var stock = request.params.stock;
			//var stock = request.headers.stock;

			console.log('value of stock on server side',stock);
			console.log('value of stock database on server side',stockDb);
			quandl.search(stock,{format: "json"},(err,response)=>{
				if(err){
					throw Boom.badRequest('cannot search in quandl database');
				}
				reply(response);
			})
		}
	},
	// {
	// 	method:'GET',
	// 	path:'stock/detail',
	// 	handler:(request,reply)=>{
	// 		var 
	// 	}
	// },
	{	// Is there is need to store the stock data in our db
		method:'GET',
		path:'/stock/NSE/{dataset}/{fromDate}/{toDate}',
		handler:(request,reply)=>{
			// Yet to implement logic whether the stock is under the NSE list or fetch the keyword corresponding to the stock
			quandl.dataset({
				source:"NSE",
				table:request.params.dataset
			},{ order : "asc", 
				exclude_column_names: false,
				start_date:request.params.fromDate,
				end_date:request.params.toDate
		},(err,response)=>{
			if(err){
				throw Boom.badRequest(err);
			}
		
			reply(response);
		})
		}
	},
	{
		method:'GET',
		path:'/BSE/{stock}',
		handler:(request,reply)=>{
			quandl.dataset({
				source:"BSE",
				table:request.params.stock
			},{ order : "asc",
				start_date:"2016-12-15",end_date:"2016-12-19"
		},(err,response)=>{
			if(err){
				throw Boom.badRequest(err);
			}
			console.log(response);
			reply(response);
		})
		}
	},
	{
		method:'GET',
		path:'/live/NIFTY',
		handler:(request,reply)=>{
			Request('http://finance.google.com/finance/info?client=ig&q=NIFTY',(err,response,body)=>{
					console.log('body',body);
					console.log('body after extraction',body[0]);
					reply(body);
					var value = body[0].l;
					console.log('value of nifty is',value);
			})
		}
	},
	{
		method:'GET',
		path:'/live/SENSEX',
		handler:(request,reply)=>{
			Request('http://finance.google.com/finance/info?client=ig&q=sensex',(err,response,body)=>{
					console.log(body);
					reply(body);
			})
		}
	}

	])
	next();
}

exports.register.attributes = {
	name : 'stock-routes',
	version: '1.0.0'
}
