const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const CommentModel = new Schema({
	"text":{ type: String },
	"postId":{ type : ObjectId , required : true },
	"isVerified":{ type: Boolean,default : false },
	"reply":[String],
	"replyTo" : {type: String},
	"user" : {
		"id" : {type : ObjectId },
		"name" : {type : String }
	}
})

module.exports= mongoose.model('Comment',CommentModel);