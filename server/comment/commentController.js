'use strict';
const Comment =require('./Comment');
const Boom = require('boom');
const Joi = require('joi');
const ObjectId = require('mongoose').Types.ObjectId;
const Post = require('../post/Post');


const username = 'admin';
const password = 'admin123';

exports.register = function(server,options,next){ 
	server.route([
	{
		method:'POST',
		path:'/post/comment',
		handler:(request,reply)=>{
			let comment = new Comment();
			comment.text = request.payload.text;
			comment.postId = new ObjectId(request.payload.postId);
			comment.authorId = request.payload.authorId;
			comment.save((err,comment)=>{
				if(err){
					throw Boom.badRequest(err);
				}
				reply(comment);
			})
		},
		config:{
			validate:{
				payload:{
					text : Joi.string().min(10).max(250),
					postId : Joi.string().required(),
					authorId : Joi.string()
				}
			}
		}
	},
	{
		method:'GET',
		path:'/comment/toverify',
		handler:(request,reply)=>{
			Comment.find({isVerified:false},(err,comments)=>{
				if(err){
					throw Boom.badRequest(err)
				}
				reply(comments)
			})
		}
	},
	{
		method:'POST',
		path:'/comment/approve',
		handler:(request,reply)=>{
				if(request.payload.username===username && request.payload.password===password){
				Comment.findByIdAndUpdate(new ObjectId(request.payload.commentId),{
					$set:{ isVerified : true } 
					},{ new : true},(err,comment)=>{
						Post.findByIdAndUpdate(new ObjectId(comment.postId),{
						$inc:{ commentCount : 1 } 
						},{new :true},(err,post)=>{ 
							if(err){
								throw Boom.badRequest(err);
								}
							})
						if(err){
							throw Boom.badRequest(err);
							}
						reply(comment);	
					})
				}
				else{
					console.log('You are not authorized to approve the comment');
					//var error =  new Error('You are not authorized to approve the post');
					//return Boom.badRequest(err);
				}

		},
		config:{
			validate:{
				payload:{
					username : Joi.string().required(),
					password : Joi.string().required(),
					commentId : Joi.string().required()
				}
			}
 		}
 	},
 	{
		method:'POST',
		path:'/comment/reply',
		handler:(request,reply)=>{
			Comment.findByIdAndUpdate(new ObjectId(request.payload.commentId),{
				$push:{reply: [request.payload.reply]}
			},{ new : true},(err,comment)=>{
				if(err){
					throw Boom.badRequest(err);
				}
				reply(comment);
			})
		},
		config:{
			validate:{
				payload:{
					commentId : Joi.string().required(),
					reply : Joi.string().required()
				}
			}
		}
	
	},
	{
		method:'GET',
		path:'/allcomment/{postId}',
		handler:(request,reply)=>{
			Comment.find({postId:request.params.postId,isVerified:true},(err,comments)=>{
				if(err){
					throw Boom.badRequest(err);
				}
				reply(comments)
			})
		}
	}

])
next();
}

exports.register.attributes = {
	name : 'comment-routes',
	version: '1.0.0'
}