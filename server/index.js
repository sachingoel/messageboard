'use strict';
const Hapi = require('hapi');
const mongoose = require('mongoose');
const Glob = require('glob');
const Path = require('path');
const User = require('./user/User');
const Asset = require('./assets/Assets');

//const verifyUserCredentials = require('./util/verifyUserCredentials');

const server = new Hapi.Server();

// Making the server connections
server.connection({
	host:'localhost',
	port:'3333',
	routes: {
		cors: {
			origin: ['/*','http://localhost:3333']
		}

	}
})

// Register the server to the hapi-auth-jwt2 plugin
server.register([
	require('hapi-auth-jwt2'),
	require('inert'),
	require('./user/userController'),
	require('./post/postController'),
	require('./assets/assetsController'),
	require('./comment/commentController')],(err)=>{
	if(err){
		throw err;
	}
});

// var myPluginOpts = {
//     connection: {
//         "host": "localhost",
//         "opts": {
//             "parser": "javascript"
//         }
//     }
// };
			
// server.register({
//     register: require('hapi-redis'),
//     options: myPluginOpts
// }, function () {
// 	if(err){
// 		console.log('failed in registering plugin')
// 	}
// });


var myvalidate = (decoded,request,callback)=>{
	//write the logic for verifying the jwt token send by the user
	if(!User[decoded.id]){
		return callback(null,false);
	}
	else{
		return callback(null,true);
	}
}

//Defining the authentication strategy for the user
server.auth.strategy('jwt', 'jwt',
    { key: 'ThIsiStHetopsECretKEy',          // private secret key
      validateFunc: myvalidate,            	//	validate function for verifying the token sent by the user
      verifyOptions: { algorithms: [ 'HS256' ],ignoreExpiration: true } // algorithm used 
  })

//Testing the routes for authentication requirements 
server.route([{
	method: 'GET',
	path: '/restricted',
	handler: (request,reply)=>{
		reply('this page is accessible to only authenticated user')
		.header("Authorization", request.headers.authorization);
	},
	config:{
		auth:{
			strategy : 'jwt'
		}
	}
}
,{
	method:'POST',
	path:'/redis',
	handler:(request,reply)=>{
		var redisClient = request.server.plugins['hapi-redis'].client;
		console.log(redisclient);
	}
}
])

//start the server
if(!module.parent){
	server.start((err)=>{
		if(err){
			throw err;
		}
//connect mongoose to mongoDB
mongoose.connect( 'mongodb://127.0.0.1/messageboard' , {} , (err)=>{
	if(err){
		throw err;
	} 
});
console.log('messageboard server is running at ', server.info.uri);
});
}

module.exports = server;
