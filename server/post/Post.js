const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
//const User = require('./User');

const PostModel = new Schema({
	"text": {type: String,trim: true,max: 2500},
	"rating": [Number],
	"like":{
		"count":{type: Number,default : 0},
		"likedBy":[{type: ObjectId}]
			},
	"offensive": {
		"count":{type: Number,default : 0},
		"offenceBy":[{type: ObjectId}]
			},
	"created_on": {type: Date,default: Date.now()},
	"commentCount": {type: Number,default :0},
	"isVerified": {type: Boolean,default: false},
	"created_on": {type: Date,default: Date.now()},
	"modified_on": {type: Date},
	"user" : {
		"id" : {type : ObjectId },
		"name" : {type : String }
	}
});

module.exports= mongoose.model('Post',PostModel);