'use strict';
//const User = require('../model/User');
const Post = require ('./Post');
const Boom = require('boom');
const Joi = require('joi');
const ObjectId = require('mongoose').Types.ObjectId;


const username = 'admin';
const password = 'admin123';

exports.register = function(server,options,next){
	server.route([
	{
		method:'POST',
		path:'/post',
		handler: (request,reply)=>{
			console.log('post before creattion(server side):',request.payload);
			//let author={};
			let post = new Post();
			//post.title = request.payload.title;
			post.text = request.payload.text;
			post.authorId =request.payload.authorId,

			post.save((err)=>{
				if(err){
					throw Boom.badRequest(err);
				}
				reply('Post Successfully');
			});
		},
		config:{
			validate:{
				payload:{
					//title : Joi.string().min(5).max(50).required(),
					text : Joi.string().min(10).max(350),
					authorId : Joi.string().required()
				}
			}
		}
	},
	{
		method:'GET',
		path:'/post/toverify',
		handler:(request,reply)=>{
			Post.find({isVerified:false},(err,posts)=>{
				if(err){
					throw Boom.badRequest(err);
				}
				reply(posts)
			})
		}
	},
	{
		method:'POST',
		path:'/post/approve',
		handler: (request,reply)=>{
			if(request.payload.username===username && request.payload.password===password){
				Post.findByIdAndUpdate(new ObjectId(request.payload.postId),{
					$set:{ isVerified : true , created_on:Date.now()} 
				},{ new : true},(err,post)=>{
					if(err){
						throw Boom.badRequest(err);
					}
					reply(post);

				})
			}
			else{
				console.log('You are not authorized to approve the post');
					//var error =  new Error('You are not authorized to approve the post');
					//return Boom.badRequest(err);
				}
			},
			config:{
				validate:{
					payload:{
						username : Joi.string().required(),
						password : Joi.string().required(),
						postId : Joi.string().required()
					}
				}
			}
		},
		{
			method:'GET',
			path:'/post/{id}',
			handler:(request,reply)=>{
				var id = request.params.id;
				Post.findOne({_id:id},(err,post)=>{
					if(err){
						throw Boom.badRequest(err);
					}
					reply(post);
				});
				
			}
		},
		{
			method:'GET',
			path:'/post/all',
			handler:(request,reply)=>{
				Post.find({isVerified:true},(err,post)=>{
					if(err){
						throw Boom.badRequest(err);				
					}
					reply(post);
				});
			}
		},
		{
			method:'POST',		
			path:'/post/like',
			handler:(request,reply)=>{
				function getUsers(postId){
					var query = Post.findOne({"_id":postId},{"like.likedBy":1,"_id":0 });
					return query;
				}

				var items=[];
				var query =getUsers(request.payload.postId);
				query.exec((err,result)=>{
					if(err){
						throw Boom.badRequest(err);
					}
					
					var items =result.like.likedBy;
					if(items.indexOf(request.payload.userId) === -1){
						Post.findByIdAndUpdate(new ObjectId(request.payload.postId),{
							$inc: {"like.count" : 1 },
							$addToSet: {"like.likedBy":request.payload.userId}
						},{new :true},(err,post)=>{ 
							if(err){
								throw Boom.badRequest(err);
							}
							reply(post)
						})
					}
					else{
						reply('You have already liked this post')
						return Boom.badRequest('You have already liked this post')
						
					}

				})

			},
			config:{
				validate:{
					payload:{
						postId : Joi.string().required(),
						userId : Joi.string().required()
					}
				}
			}

		},
		{
			method: 'POST',		
			path:'/post/offensive',
			handler: (request,reply)=>{
				function getUsers(postId){
					var query = Post.findOne({"_id":postId},{"offensive.offenceBy":1,"_id":0 });
					return query;
				}
				var items=[];
				var query =getUsers(request.payload.postId);
				query.exec((err,result)=>{
					if(err){
						throw Boom.badRequest(err);
					}
					var items =result.offensive.offenceBy;
					if(items.indexOf(request.payload.userId) === -1){
						Post.findByIdAndUpdate(new ObjectId(request.payload.postId),{
							$inc : { "offensive.count" : 1 },
							$addToSet: {"offensive.offenceBy":request.payload.userId}
						},{new : true},(err,post)=>{
							if(err){
								throw Boom.badRequest(err);
							}
							reply(post);	
						})
					}
					else{
						reply('You have already offence this post')
						return Boom.badRequest('You have already offence this post')
					}
				})
			},
			config:{
				validate:{
					payload:{
						postId : Joi.string().required(),
						userId : Joi.string().required()
					}
				},
			}
		},
		{
			method:'POST',
			path:'/post/rate',
			handler: (request,reply)=>{
				Post.findByIdAndUpdate(new ObjectId(request.payload.postId),{
					$push : { rating : [request.payload.rating] }
				},{ new: true },(err,post)=>{
					if(err){
						throw Boom.badRequest(err);
					}
					reply(post);
				})
			},
			config:{
				validate:{
					payload:{
						postId : Joi.string().required(),
						rating : Joi.number().integer().min(1).max(5).required()
					}
				}
			}
		},
		{
			method: 'GET',		
			path: '/post/popular',
			handler: (request,reply)=>{
				Post.find({isVerified:true},null,{sort:{commentCount:-1}},(err,post)=>{
					if(err){
						throw Boom.badRequest(err);				
					}
					reply(post);
				});
			}
		},
		{
			method:"GET",
			path:"/post/recent",
			handler:(request,reply)=>{
				Post.find({isVerified:true},null,{sort:{created_on:-1}},(err,post)=>{
					if(err){
						throw Boom.badRequest(err);				
					}
					reply(post);
				});
			}
		}
		])
next();

} 

exports.register.attributes = {
	name : 'post-routes',
	version: '1.0.0'
}
