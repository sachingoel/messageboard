const Lab = require('lab');
const Code = require('code');
const lab = exports.lab = Lab.script();
const server = require('../index.js');

lab.experiment('User',()=> {
    lab.test('homepage',(done)=> {
    var options = {
        method: 'GET',
        url: '/'
    	};
    	server.inject(options,(res)=> {
        	var result = res.result;
        	Code.expect(res.statusCode).to.equal(200);
        	Code.expect(result).to.equal('landing on messageboard home page');
        	done();
    		});
	});

    lab.test('register',(done)=>{
    	var options = {
    		method: 'POST',
    		url:'/register',
    		payload:{
    			username: 'sachingoel',
    			email: 'sachingoel@gmail.com',
    			password:'sachin123'
    		}
    	};
    	server.inject(options,(res)=>{
    		var result = res.result;
    		Code.expect(res.statusCode).to.equal(200);
    		Code.expect(res.result).to.equal('registered sucessfully');
    		done();
    		});
    });
});