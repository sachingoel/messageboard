const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var addressSubSchema = {
	"address": String,
	"city": String,
	"state": String,
	"country": String,
	"zipcode" : Number
}
const UserModel = new Schema({
	"name":{ type:String },
	"username":{type:String,required:true,index:{ unique:true }},
	"email":{type:String,required:true,index:{ unique:true }},
	"password":{type:String,required:true},
	"DOB":{type:Date},
	"scope":{type: String},
	"address": addressSubSchema,
	"created_on":{type: Date,default: Date.now()},
	"modified_on":{type: Date},
	"posts":[{
		type:ObjectId,
		ref: 'Post'
	}]

});

module.exports= mongoose.model('User',UserModel);
