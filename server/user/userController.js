'use strict';
const Joi = require('joi');
const User = require('./User');
const bcrypt = require('bcryptjs');
const verifyUniqueUser = require('./util/verifyUniqueUser');
const verifyUserCredentials = require('./util/verifyUserCredentials');
const createToken = require('./util/generateToken');


function hashPassword(password, cb) {
  // Generating a salt at level 10 
  bcrypt.genSalt(7, (err, salt) => {
    bcrypt.hash(password, salt, (err, hash) => {
      return cb(err, hash);
    });
  });
}




exports.register = (server,options,next)=>{

 server.route([
	{
		method:'GET',
		path:'/',
		handler:(request,reply)=>{
			reply('landing on messageboard home page');
		}
	},
	{
		method:'POST',
		path:'/register',
		config:{
			auth:false,
			pre:[
				{ method:verifyUniqueUser }
			],
			handler:(request,reply)=>{

			let user = new User();
			user.name = request.payload.name;
			user.username = request.payload.username;
			user.email = request.payload.email;
			hashPassword(request.payload.password, (err, hash) => {
        			if (err) {
          				throw Boom.badRequest(err);
       						 }
       				user.password = hash;
        			user.save((err, user) => {
         				if (err) {
            			throw Boom.badRequest(err);
          				}
          			reply('registered sucessfully');
        			});
      			});
			},
			validate:{
				 payload: {
				 	name: Joi.string(),
        			username: Joi.string().min(5).max(20).required(),
       				email: Joi.string().email().required(),
       				password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required()
      				}
				}
		}
	},
	{
		method:'POST',
		path:'/login',
		config:{
			auth:false,
			pre:[
				{ method:verifyUserCredentials }
			],
			handler:(request,reply)=>{
				console.log(request.pre.jsonreply);
				reply(request.pre.user);
			},
			validate:{
				 payload: {
        			username: Joi.string().min(5).max(20).required(),
       				password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required()
      				}
				}
		}

	},
	// {
	// 	method:'GET',
	// 	path:'/restricted',
	// 	handler:(request,reply)=>{
	// 		reply('This area is accessible to only authenticated user with the help of tokens')
	// 		.header("Authorization", request.headers.authorization);
	// 	},
	// 	config:{
	// 		auth:{
	// 			strategy :'jwt'
	// 		}
	// 	}
	// },
	{
		method:'POST',
		path:'/logout',
		handler:(request,reply)=>{
			reply('you are now logged out');
		}
	}
])
 next();

}

exports.register.attributes = {
	name : 'user-routes',
	version: '1.0.0'
}