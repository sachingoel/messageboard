'use strict'; 
const boom = require('boom');
const User = require('../User');

const verifyUniqueUser = (req,res)=>{
	
	User.findOne({ $or: [ 
			{ email : req.payload.email },
			{ username : req.payload.username}
		]}, (err,user)=>{
				
				if(err){
					throw err;
				}

				if(user){
					if(user.username === req.payload.username){
						res(boom.badRequest('Username already exist'));
						return;
					}
					if(user.email === req.payload.email){
						res(boom.badRequest('Email already exist'));
						return;
					}
				}

			res(req.payload);
		});
	}

module.exports = verifyUniqueUser ;