'use strict';
const User = require('../User');
const boom = require('boom');
const bcrypt = require('bcryptjs');
const createToken = require('./generateToken');

var verifyUserCredentials = (req,res)=>{

const password = req.payload.password;
	
	User.findOne({ $or: [ 
			{ username : req.payload.username}
		]}, (err,user)=>{
				if(err){
				throw err;
			}
			if(user){
				 bcrypt.compare(password, user.password, (err, isValid) => {
       			 if (isValid === true) {
       			 	
       			 	
          			var token = createToken(user);
          			
          			var userresponse ={
          				"sucess" : true,
          				"data" : user,
          				"token" : token
          			}
          				res(userresponse).code(200)
        			}
        			else {
          				res(boom.badRequest('Password is Wrong'));
        				}
     			 });
			}
			else{
				res(boom.badRequest('Username does not exist'))
			}
	});
}

module.exports = verifyUserCredentials;